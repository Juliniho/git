# GIT

## ARQUITECTURA

3 estados

- working directory W.D:: donde se trabaja con los files

    - GIT ADD

- staging area S.A:: donde se agrega los files para el pre-guardado

    - GIT COMMIT

- repository local:: se sube al repositorio Local

    - GIT PUSH

- repository remoto:: GITHUB o GITLAB

## MAS USADOS

* Ver el estado del repositorio antes de aplicar un merge
> git log --oneline --decorate --all --graph

## COMANDOS 

> git init <crea "proyecto nuevo en local P.e el directory .git>

> git remote add origin <"link> <donde se "almacena el codigo remoto, esto va antes del push y despues del commit>

> git add <"file> <pasar "files del WorkinDirect al StaginArea>

> git checkout -- <"file> <descarta los "cambios en un archivo>

>> git checkout <name "rama o version> <permite "activar la rama o version primero mirar el git branch>

> git diff <"file> <show los "cambios en un file>

> git status <show "status de los files en W.D o S.A y tambien muestra todo cambio en el codigo>

> git commit -m <"messg> <Pasar-"GUARDA files del S.A al REPOSITORIO TEMP-LOCAL "REALIZA-UN-SNAPSHOT" antes de subirlo al REPOSITORIO REMOTO>

> git log --oneline --decorate --all --graph <Permite "seguir el hash generado para el commint>

> git push -u origin master <subir a "repositorio remoto, la rama master luego se puede cambiar>

> git pull <bajar "cambios de un repositorio remoto>

>> > git fetch origin <pregunta "repositorio remoto si hay novedades y las descarga, como el pull>

> git clone 

> git branch <visualiza la "rama o version>

> git reset <reinicia "todo>

>> git branch <nom-"version-rama> <permite "crear ramas o versiones del codigo>

> add file para ignorar archivos .gitignore y metemos alli la ruta de los directorios o archivos a ignorar en los git adds y comprobar con el git status

> git stash <El "comando git stash se utiliza para almacenar temporalmente el trabajo no confirmado con el fin de limpiar el directorio de trabajo sin tener que confirmar el trabajo no acabado en una rama.Básicamente esto es enteramente cubierto en Guardado rápido y Limpieza.>


> git tag <El "comando git tag se utiliza para dar un marcador permanente a un punto específico en el historial del código fuente. Generalmente esto se utiliza para cosas como las liberaciones (releases).Este comando se introduce y se trata en detalle en Etiquetado y lo usamos en la práctica en Etiquetando tus versiones.También cubrimos cómo crear una etiqueta con firma GPG tag con el indicador -s y verificamos uno con el indicador -v en Firmando tu trabajo.>


---

## COMANDOS MAS USADOS
### Descartar los cambios de un fichero
> git checkout -- <nameFile> 
### ver los cambios del fichero en verde los nuevos cambios
> git diff <nameFile>


---
# scope 
------------------
working directory (local)
------------------
* ver traza de cambios de los commits(ver tambien los hash)
> git log 
* ver estado
> git status
* ver cambios del contenido
> git diff 
* volver atraz un fichero-deshacer cambios al original (antes de ir al Staged Area) usar el git diff para ver los cambios
> git checkout -- <nameFile> 
* registrar cambios del working directory al Staged Area, previo commit
> git add <nameFile> o <.>
------------------
Staged Area (area temporal para guardar cambios)
------------------
* revertir cambios dentro de la misma branch del Staged Area al working directory (volver atraz y cambia el hash)
> git commit --amend
* reset para volver atraz (al working directory), Unstaged, pero luego abria que hacer un git checkout -- <nameFile>
> git reset HEAD <nameFile>
* ver hash y mensaje del commit
> git log --oneline
---
* revertir un commit, necesitamos el hash (`cuidado porque borra el commit current`) y lo deja en el working directory editado(conserva los cambios locales), usar el git diff para mas detalle
> git reset <codigoCommitAVolver>
* revertir un commit, borra todo contando los Staged y working directory (`cuidado porque borra el commit current`)
> git reset --hard <codigoCommitAVolver>
* revertir un commit, lo deja hasta el Staged Area (`cuidado porque borra el commit current`)
> git reset --soft <codigoCommitAVolver>
---
* revertir (`mejor opcion que el reset con git compartidos`), descarta los cambios en un commit, pero no borra el commit o hash, importante utilizar diff para ver los cambios revertidos, `Se queda en el estado Staged Area previo commit`
- primero ver diferencia entre commit
> git diff <codigoCommitAVolver> <codigoCommitCurrent>
* ver hash,mensaje del commit y sobre todo el flat o puntero del commmit actual
> git log --oneline --decorate
* ver un commit anterior, con la letra tilde = anterior (~) dice el commit + en numero al que queremos retroceder, este commando compara el commit ultimo commit con su anterior por puntero HEAD
> git diff <anteriorAlUtimoCommit>HEAD~1 <ultimoCommit>HEAD
* retroceder ultimo commit por puntero HEAD `Se queda en el estado Staged Area previo commit`
> git revert HEAD
* deshacer 2 cambios o mas commits `Se queda en el estado Staged Area previo commit`
> git revert --no-commit HEAD </br>
> git revert --no-commit HEAD~1 </br>
> git revert --continue
------------------
Remoto
------------------
* ver hash,mensaje del commit,puntero y todo el codigo mas sus trazas
> git log --oneline --decorate --all --graph
* Fusion de 1 rama a master(Teniendo encuenta que estamos en la branch que vamos a unir a master), lo hace con un fastforward.
> git checkout master  </br>
> git pull origin master </br>
> git merge <ramaDeDondeVieneLaActualizacionRamaOrigen> </br>
> git push origin master
* Fusion de 2 rama a master(estrategia recursiva `si no genera conflicto es automatico`)
> git log --oneline --decorate --all --graph
> git checkout master  </br>
> git pull origin master </br>
> git merge <ramaDeDondeVieneLaActualizacionRamaOrigen 1> </br>
> git merge <ramaDeDondeVieneLaActualizacionRamaOrigen 2> </br>
> git push origin master
* conflicto en el merge con el master,ver el fichero donde esta el conflicto,lo arreglo y vuelvo a enviarlo a la region staged Area y en el commit mostrara el mensaje del conflicto
> git status </br>
> git add <nombreFichero>
> git commit 
* conflicto (si no tenemos claro como arreglar el conflicto ahora, abortamos el merge)
> git merge --abort 